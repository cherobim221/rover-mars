Rover Mars Js

In the data.js file, it will be where the application data should be filled, such as the number of rovers and the size of the plateu

To run the program just run

- node rover.js

The console will show the answer showing the updated plate and the final positions of the rovers

Tests

> Landing Position: 1 2 N
> Instruction: LMLMLMLMM
> Final Position: 1 3 N

> Landing Position: 3 3 E
> Instruction: MRRMMRMRRM
> Final Position: 2 3 S
