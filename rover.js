import data from "./data.json" assert { type: "json" };

function move(direction, position) {
  let { x, y, orientation } = position;

  //check the direction and the orientation for the command
  if (direction == "L") {
    if (orientation == "N") {
      orientation = "W";
    } else if (orientation == "W") {
      orientation = "S";
    } else if (orientation == "S") {
      orientation = "E";
    } else if (orientation == "E") {
      orientation = "N";
    }
  }

  if (direction == "R") {
    if (orientation == "N") {
      orientation = "E";
    } else if (orientation == "W") {
      orientation = "N";
    } else if (orientation == "S") {
      orientation = "W";
    } else if (orientation == "E") {
      orientation = "S";
    }
  }

  //change the position off rover
  if (direction === "M" && orientation == "N") {
    y += 1;
  } else if (direction === "M" && orientation == "W") {
    x -= 1;
  } else if (direction === "M" && orientation == "S") {
    y -= 1;
  } else if (direction === "M" && orientation == "E") {
    x += 1;
  }

  return { x, y, orientation };
}
const generatePlato = (size) => {
  var matrix = [];
  for (var i = 0; i < size; i++) {
    matrix[i] = new Array(size).fill(0);
  }
  return matrix;
};
//start the application
function start(r, p) {
  let rovers = r;
  let plateau = generatePlato(p);

  try {
    //set the rovers in plateau
    rovers.forEach((e) => {
      plateau[e.y][e.x] = 1;
    });

    rovers.forEach((rover) => {
      //set commands string in array
      let rover_commands = rover.commands.split("");

      //loop through the command list
      rover_commands.forEach((command) => {
        if (command !== "L" && command !== "R" && command !== "M") {
          console.log("Invalid Command: ", command);
          return;
        }

        //saves the actual position off rover
        const lastPostision = {
          x: rover.x,
          y: rover.y,
          orientation: rover.orientation,
        };

        //call the moviment method
        const moviment = move(command, {
          x: rover.x,
          y: rover.y,
          orientation: rover.orientation,
        });

        //update the orientation off rover
        rover.orientation = moviment.orientation;

        //makes the moviment
        if (command === "M") {
          plateau[lastPostision.y][lastPostision.x] = 0;
          rover.x = moviment.x;
          rover.y = moviment.y;
          plateau[rover.y][rover.x] = 1;
        }
      });
    });
    console.log("* ---- *");
    console.log(rovers);
    console.log(plateau);
    console.log("* ---- *");
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
}

start(data.rovers, data.plateau);
